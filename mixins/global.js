export default {
  data() {
    return {
      fixedTop: true
    }
  },
  methods: {
    goHome() {
      this.$router.push("/");
    }
  }
}