import pkg from './package.json'
export default {
  target: 'static',
  env: {
    tutorialUrl: process.env.TUTORIAL_URL || 'https://tube-numerique-educatif.apps.education.fr/c/tutoriels/videos?s=1',
    tutorialVideosUrl : process.env.TUTORIAL_VIDEOS_URL || "https://tube-numerique-educatif.apps.education.fr/api/v1/video-channels/tutoriels/videos?start=0&count=5&sort=-publishedAt&skipCount=true&nsfw=false"
  },
  head: {
    title: `Tubes - ${pkg.version}`,
    htmlAttrs: {
      lang: 'fr'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  css: [
    './styles/main.scss'
  ],
  styleResources: {
    scss: [
      './styles/_variables.scss',
      './styles/_mixins.scss'
    ]
  },

  plugins: [
    { src: '~plugins/app.js', mode: 'client' },
    '~plugins/i18n.js',
    '~plugins/mq.js',
    '~plugins/click-outside.js',  
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    {
      path: '~/components', // will get any components nested in let's say /components/test too
      pathPrefix: false,
    },
  ],
  buildModules: [
    //'@nuxtjs/eslint-module'
    '@nuxtjs/style-resources',
    ['@nuxtjs/fontawesome', {
      component: 'fa',
      icons: {
        solid: [
          'faTimes',
          'faArrowLeft',
          'faChevronRight',
          'faPlus',
          'faMinus'
        ]
      }
    }]
  ],
  modules: [
  ],

  build: {
  }
}
