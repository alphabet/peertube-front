export default function scrollBehavior (to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition;
  }
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (to.hash) resolve({ selector: to.hash });
      resolve({ x: 0, y: 0 })
    }, 50)
  })
}