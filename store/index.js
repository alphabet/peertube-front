
export const state = () => ({
  formSearch: '',
  searchCounter: 0
});

export const mutations = {
  setFormSearch(state, formSearch) {
    state.formSearch = formSearch;
  },
  incrementSearchCounter(state) {
    state.searchCounter = state.searchCounter + 1;
  }
};