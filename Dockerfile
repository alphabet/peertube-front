FROM hub.eole.education/proxyhub/library/node:16-alpine AS build

RUN mkdir /src
COPY . /src
WORKDIR /src

RUN yarn install
RUN yarn generate

FROM hub.eole.education/proxyhub/library/nginx:alpine

COPY --from=build /src/dist /usr/share/nginx/html
