import axios from 'axios'
import { buildApiUrl } from './utils'

const basePath = '/api/v1/config'
let serverConfigPromise;

function getConfigHttp () {
  return axios.get(buildApiUrl(basePath))
}

async function loadServerConfig () {
  const res = await getConfigHttp()
  return res.data
}

function getConfig () {
  if (serverConfigPromise) return serverConfigPromise
  serverConfigPromise = loadServerConfig()
  return serverConfigPromise
}

export {
  getConfig,
  loadServerConfig
}
