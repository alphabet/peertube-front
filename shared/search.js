import axios from 'axios'
import { buildApiUrl } from './utils'

const baseVideosPath = '/api/v1/search/videos'
const baseVideoChannelsPath = '/api/v1/search/video-channels'
const baseVideoPlaylistsPath = '/api/v1/search/video-playlists'

function searchVideos (params) {
  const options = {
    params
  }

  if (params.search) Object.assign(options.params, { search: params.search })
  return axios.get(buildApiUrl(baseVideosPath), options).then(res => res.data)
}

function searchVideoChannels (params) {
  const options = {
    params
  }

  if (params.search) Object.assign(options.params, { search: params.search })

  return axios.get(buildApiUrl(baseVideoChannelsPath), options).then(res => res.data)
}

function getTutorialsVideos (url) {
  return axios.get(url).then(res => res.data)
}

function searchVideoPlaylists (params) {
  const options = {
    params
  }
  if (params.search) Object.assign(options.params, { search: params.search })
  return axios.get(buildApiUrl(baseVideoPlaylistsPath), options).then(res => res.data)
}

export {
  searchVideos,
  searchVideoChannels,
  searchVideoPlaylists,
  getTutorialsVideos,
}
