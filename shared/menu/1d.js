
const maternelle = {
  "id": "maternelle",
  "label": "Maternelle",
  "children": {
    "mobiliser-le-langage-dans-toutes-ses-dimensions": {
      "id": "mobiliser-le-langage-dans-toutes-ses-dimensions",
      "categoryId": 42,
      "label": "Mobiliser le langage dans toutes ses dimensions"
    },
    "agir-sexprimer-comprendre-a-travers-lactivite-physique": {
      "id": "agir-sexprimer-comprendre-a-travers-lactivite-physique",
      "categoryId": 44,
      "label": "Agir, s'exprimer, comprendre à travers l'activité physique"
    },
    "agir-sexprimer-comprendre-a-travers-les-activites-artistiques": {
      "id": "agir-sexprimer-comprendre-a-travers-les-activites-artistiques",
      "categoryId": 46,
      "label": "Agir, s'exprimer, comprendre à travers les activités artistiques"
    },
    "construire-les-premiers-outils-pour-structurer-sa-pensee": {
      "id": "construire-les-premiers-outils-pour-structurer-sa-pensee",
      "categoryId": 48,
      "label": "Construire les premiers outils pour structurer sa pensée"
    },
    "explorer-le-monde": {
      "id": "explorer-le-monde",
      "categoryId": 50,
      "label": "Explorer le monde"
    }
  }
};

const cycle2 = {
  "id": "cycle2",
  "label": "Cycle 2",
  "children": {
    "francais": {
      "id": "francais",
      "categoryId": 52,
      "label": "Français"
    },
    "langues-vivantes": {
      "id": "langues-vivantes",
      "categoryId": 54,
      "label": "Langues vivantes"
    },
    "arts-plastiques": {
      "id": "arts-plastiques",
      "categoryId": 56,
      "label": "Arts plastiques"
    },
    "education-musicale": {
      "id": "education-musicale",
      "categoryId": 58,
      "label": "Éducation musicale"
    },
    "education-physique-et-sportive": {
      "id": "education-physique-et-sportive",
      "categoryId": 60,
      "label": "Éducation physique et sportive"
    },
    "enseignement-moral-et-civique": {
      "id": "enseignement-moral-et-civique",
      "categoryId": 62,
      "label": "Enseignement moral et civique"
    },
    "questionner-le-monde": {
      "id": "questionner-le-monde",
      "categoryId": 64,
      "label": "Questionner le monde"
    },
    "mathematiques": {
      "id": "mathematiques",
      "categoryId": 66,
      "label": "Mathématiques"
    }
  }
};

const cycle3 = {
  "id": "cycle3",
  "label": "Cycle 3",
  "children": {
    "francais": {
      "id": "francais",
      "categoryId": 52,
      "label": "Français"
    },
    "langues-vivantes": {
      "id": "langues-vivantes",
      "categoryId": 54,
      "label": "Langues vivantes"
    },
    "arts-plastiques": {
      "id": "arts-plastiques",
      "categoryId": 56,
      "label": "Arts plastiques"
    },
    "education-musicale": {
      "id": "education-musicale",
      "categoryId": 58,
      "label": "Éducation musicale"
    },
    "education-physique-et-sportive": {
      "id": "education-physique-et-sportive",
      "categoryId": 60,
      "label": "Éducation physique et sportive"
    },
    "enseignement-moral-et-civique": {
      "id": "enseignement-moral-et-civique",
      "categoryId": 62,
      "label": "Enseignement moral et civique"
    },
    "histoire-geographie": {
      "id": "histoire-geographie",
      "categoryId": 68,
      "label": "Histoire-Géographie"
    },
    "histoire-des-arts": {
      "id": "histoire-des-arts",
      "categoryId": 70,
      "label": "Histoire des arts"
    },
    "mathematiques": {
      "id": "mathematiques",
      "categoryId": 66,
      "label": "Mathématiques"
    },
    "questionner-le-monde": {
      "id": "questionner-le-monde",
      "categoryId": 64,
      "label": "Questionner le monde"
    },
    "sciences-et-technologie": {
      "id": "sciences-et-technologie",
      "categoryId": 67,
      "label": "Sciences et technologie"
    }
  }
};

export default {
  "id": "1d",
  "label": "Premier degré",
  "hint": "Cliquez pour afficher les rubriques du premier degré",
  "children": {
    "maternelle": maternelle,
    "cycle2": cycle2,
    "cycle3": cycle3
  }
};