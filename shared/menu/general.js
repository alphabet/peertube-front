const actionEducative = {
  "id": "action-educative",
  "label": "Action éducative",
  "children": {
    "reussite-educative": {
      "id": "reussite-educative",
      "categoryId": 212,
      "label": "Réussite éducative"
    },
    "ecole-inclusive": {
      "id": "ecole-inclusive",
      "categoryId": 215,
      "label": "École inclusive"
    },
    "parcours-deducation-artistique-et-culturelle": {
      "id": "parcours-deducation-artistique-et-culturelle",
      "categoryId": 218,
      "label": "Parcours d'éducation artistique et culturelle"
    },
    "parcours-citoyen": {
      "id": "parcours-citoyen",
      "categoryId": 221,
      "label": "Parcours Citoyen"
    },
    "parcours-avenir": {
      "id": "parcours-avenir",
      "categoryId": 224,
      "label": "Parcours Avenir"
    },
    "education-au-developpement-durable": {
      "id": "education-au-developpement-durable",
      "categoryId": 227,
      "label": "Education au développement durable"
    },
    "ecole-promotrice-de-sante": {
      "id": "ecole-promotrice-de-sante",
      "categoryId": 228,
      "label": "École promotrice de santé"
    },
    "ouverture-europeenne-et-internationale": {
      "id": "ouverture-europeenne-et-internationale",
      "categoryId": 230,
      "label": "Ouverture européenne et internationale"
    }
  }
};


const numeriqueEducatif = {
  "id": "numerique-educatif",
  "label": "Numérique éducatif",
  "children": {
    "enseigner": {
      "id": "enseigner",
      "categoryId": 233,
      "label": "Enseigner "
    },
    "piloter": {
      "id": "piloter",
      "categoryId": 236,
      "label": "Piloter"
    },
    "former-accompagner": {
      "id": "former-accompagner",
      "categoryId": 239,
      "label": "Former-Accompagner "
    },
    "experimenter": {
      "id": "experimenter",
      "categoryId": 242,
      "label": "Expérimenter"
    },
    "responsabiliser": {
      "id": "responsabiliser",
      "categoryId": 245,
      "label": "Responsabiliser"
    },
    "ressources-tutoriels": {
      "id": "ressources-tutoriels",
      "categoryId": 248,
      "label": "Ressources-Tutoriels"
    }
  }
};

const institutionnel = {
  "id": "institutionnel",
  "label": "Institutionnel",
  "children": {
    "ministere": {
      "id": "ministere",
      "categoryId": 251,
      "label": "Ministère"
    },
    "auvergnes-rhone-alpes": {
      "id": "auvergnes-rhone-alpes",
      "categoryId": 254,
      "label": "Auvergnes-Rhône-Alpes"
    },
    "bourgogne-franche-comte": {
      "id": "bourgogne-franche-comte",
      "categoryId": 257,
      "label": "Bourgogne-Franche-Comté"
    },
    "bretagne": {
      "id": "bretagne",
      "categoryId": 260,
      "label": "Bretagne"
    },
    "centre-val-de-loire": {
      "id": "centre-val-de-loire",
      "categoryId": 263,
      "label": "Centre-val-de-Loire"
    },
    "corse": {
      "id": "corse",
      "categoryId": 266,
      "label": "Corse"
    },
    "grand-est": {
      "id": "grand-est",
      "categoryId": 269,
      "label": "Grand-est"
    },
    "guadeloupe": {
      "id": "guadeloupe",
      "categoryId": 272,
      "label": "Guadeloupe"
    },
    "guyane": {
      "id": "guyane",
      "categoryId": 275,
      "label": "Guyane"
    },
    "hauts-de-france": {
      "id": "hauts-de-france",
      "categoryId": 278,
      "label": "Hauts-de-France"
    },
    "ile-de-france": {
      "id": "ile-de-france",
      "categoryId": 281,
      "label": "Île-de-France"
    },
    "la-reunion": {
      "id": "la-reunion",
      "categoryId": 284,
      "label": "La Réunion"
    },
    "martinique": {
      "id": "martinique",
      "categoryId": 287,
      "label": "Martinique"
    },
    "mayotte": {
      "id": "mayotte",
      "categoryId": 290,
      "label": "Mayotte"
    },
    "normandie": {
      "id": "normandie",
      "categoryId": 293,
      "label": "Normandie"
    },
    "nouvelle-aquitaine": {
      "id": "nouvelle-aquitaine",
      "categoryId": 296,
      "label": "Nouvelle-Aquitaine"
    },
    "nouvelle-caledonie": {
      "id": "nouvelle-caledonie",
      "categoryId": 299,
      "label": "Nouvelle-Calédonie"
    },
    "occitanie": {
      "id": "occitanie",
      "categoryId": 302,
      "label": "Occitanie"
    },
    "paca": {
      "id": "paca",
      "categoryId": 305,
      "label": "PACA"
    },
    "pays-de-la-loire": {
      "id": "pays-de-la-loire",
      "categoryId": 308,
      "label": "Pays-de-la-Loire"
    },
    "polynesie-francaise": {
      "id": "polynesie-francaise",
      "categoryId": 311,
      "label": "Polynésie française"
    },
    "saint-pierre-et-miquelon": {
      "id": "saint-pierre-et-miquelon",
      "categoryId": 314,
      "label": "Saint-Pierre-et-Miquelon"
    },
    "wallis-et-futuna": {
      "id": "wallis-et-futuna",
      "categoryId": 317,
      "label": "Wallis-et-Futuna"
    }
  }
};

export default {
  "id": "general",
  "label": "Généralistes",
  "hint": "Cliquez pour afficher les rubriques générales",
  "children": {
    "action-educative": actionEducative,
    "numerique-educatif": numeriqueEducatif,
    "institutionnel": institutionnel
  }
};