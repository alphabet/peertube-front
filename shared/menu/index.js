// Structure du menu accéder

import menu1d from "./1d";
import menu2d from "./2d";
import general from "./general";



export const links = {
  menu1d: {
    maternelle: "https://tube-maternelle.apps.education.fr",
    cycle2: "https://tube-cycle-2.apps.education.fr",
    cycle3: "https://tube-cycle-3.apps.education.fr"
  },
  menu2d: {
    ["arts-lettres-sciences-humaines"]: "https://tube-arts-lettres-sciences-humaines.apps.education.fr",
    ["sciences-technologies"]: "https://tube-sciences-technologies.apps.education.fr",
    ["langues-vivantes"]: "https://tube-langues-vivantes.apps.education.fr",
    ["enseignement-professionnel"]: "https://tube-enseignement-professionnel.apps.education.fr",
    ["education-physique-sportive"]: "https://tube-education-physique-et-sportive.apps.education.fr"
  },
  general: {
    ["action-educative"]: "https://tube-action-educative.apps.education.fr",
    ["numerique-educatif"]: "https://tube-numerique-educatif.apps.education.fr",
    ["institutionnel"]: "https://tube-institutionnel.apps.education.fr"
  }
};

const mapSearchLink = (link, params) => {
  const urlParams = new URLSearchParams(params);
  return `${link}/search?${urlParams.toString()}`;
}

const mapLinks = (items, links, describe = false) => {
  let mapping = [];
  Object.keys(items).forEach((itemId) => {
    if (!links[itemId]) return false;
    if (!items[itemId].children) return false;
    for (const linkId in links[itemId]) {
      if (items[itemId].children[linkId]) {
        const currentLink = links[itemId][linkId];
        let currentItem = items[itemId].children[linkId];
        currentItem.link = currentLink;
        if (currentItem.children) {
          currentItem.children = Object.keys(currentItem.children).map((childId) => {
            let child = currentItem.children[childId];
            child.link = mapSearchLink(currentLink, {
              categoryOneOf: child.categoryId,
              // searchTarget: "local"
            });
            if (describe) {
              mapping.push({
                rubrique: itemId,
                instance: currentItem.label, 
                label: child.label,
                instanceLien: currentLink,
                identifiant: child.categoryId ? child.categoryId : "N/A",
                lienRecherche: child.link
              });
            }
            return child;
          });
        }
      }
    }
  });
  return items;

}

export default mapLinks({
  menu1d,
  menu2d,
  general
}, links);