


const artsLettresSciencesHumaines = {
  "id": "arts-lettres-sciences-humaines",
  "label": "Arts & sciences humaines",
  "children": {
    "arts-plastiques": {
      "id": "arts-plastiques",
      "categoryId": 56,
      "label": "Arts plastiques"
    },
    "documentation": {
      "id": "documentation",
      "categoryId": 72,
      "label": "Documentation"
    },
    "musique": {
      "id": "musique",
      "categoryId": 74,
      "label": "Musique"
    },
    "histoire-geographie": {
      "id": "histoire-geographie",
      "categoryId": 68,
      "label": "Histoire-Géographie"
    },
    "enseignement-moral-et-civique": {
      "id": "enseignement-moral-et-civique",
      "categoryId": 62,
      "label": "Enseignement moral et civique"
    },
    "lettres": {
      "id": "lettres",
      "categoryId": 76,
      "label": "Lettres"
    },
    "langues-et-culture-de-lantiquite": {
      "id": "langues-et-culture-de-lantiquite",
      "categoryId": 78,
      "label": "Langues et culture de l'Antiquité"
    },
    "theatre": {
      "id": "theatre",
      "categoryId": 80,
      "label": "Théâtre"
    },
    "cinema-audiovisuel": {
      "id": "cinema-audiovisuel",
      "categoryId": 82,
      "label": "Cinéma et audiovisuel"
    }, 
    "philosophie": {
      "id": "philosophie",
      "categoryId": 86,
      "label": "Philosophie"
    },
    "sciences-economiques-et-sociales": {
      "id": "sciences-economiques-et-sociales",
      "categoryId": 89,
      "label": "Sciences économiques et sociales"
    }
  }
};

const sciencesTechnologies = {
  "id": "sciences-technologies",
  "label": "Sciences & technologies",
  "children": {
    "mathematiques": {
      "id": "mathematiques",
      "categoryId": 66,
      "label": "Mathématiques"
    },
    "economie-et-gestion": {
      "id": "economie-et-gestion",
      "categoryId": 92,
      "label": "Économie et gestion"
    },
    "sciences-de-la-vie-et-de-la-terre": {
      "id": "sciences-de-la-vie-et-de-la-terre",
      "categoryId": 95,
      "label": "Sciences de la vie et de la Terre"
    },
    "mathematiques-physique-chimie-en-lycee-professionnel": {
      "id": "mathematiques-physique-chimie-en-lycee-professionnel",
      "categoryId": 110,
      "label": "Mathématiques Physique Chimie en lycée professionnel"
    },
    "sante-environnement-sciences-medico-sociales": {
      "id": "sante-environnement-sciences-medico-sociales",
      "categoryId": 107,
      "label": "Santé Environnement Sciences médico-sociales"
    },
    "SNT": {
      "id": "SNT",
      "categoryId": 96,
      "label": "Sciences Numériques et Technologie (SNT)"
    },
    "NSI": {
      "id": "NSI",
      "categoryId": 97,
      "label": "Numérique et Sciences Informatiques (NSI)"
    },
    "physique-chimie": {
      "id": "physique-chimie",
      "categoryId": 98,
      "label": "Physique-Chimie"
    },
    "biotechnologies": {
      "id": "biotechnologies",
      "categoryId": 101,
      "label": "Biotechnologies"
    },
    "technologie": {
      "id": "technologie",
      "categoryId": 102,
      "label": "Technologie"
    },
    "sciences-de-l-ingénieur": {
      "id": "sciences-de-l-ingénieur",
      "categoryId": 103,
      "label": "Sciences de l'ingénieur"
    },
    "sciences-et-technologies-sanitaires-et-sociales": {
      "id": "sciences-et-technologies-sanitaires-et-sociales",
      "categoryId": 104,
      "label": "Sciences et technologies sanitaires et sociales"
    },
    "sciences-et-techniques-industrielles": {
      "id": "sciences-et-techniques-industrielles",
      "categoryId": 188,
      "label": "Sciences et techniques industrielles"
    }
  }
};

const languesVivantes = {
  "id": "langues-vivantes",
  "label": "Langues vivantes",
  "children": {
    "allemand": {
      "id": "allemand",
      "categoryId": 113,
      "label": "Allemand"
    },
    "anglais": {
      "id": "anglais",
      "categoryId": 116,
      "label": "Anglais"
    },
    "espagnol": {
      "id": "espagnol",
      "categoryId": 119,
      "label": "Espagnol"
    },
    "italien": {
      "id": "italien",
      "categoryId": 122,
      "label": "Italien"
    },
    "arabe": {
      "id": "arabe",
      "categoryId": 125,
      "label": "Arabe"
    },
    "russe": {
      "id": "russe",
      "categoryId": 128,
      "label": "Russe"
    },
    "chinois": {
      "id": "chinois",
      "categoryId": 131,
      "label": "Chinois"
    },
    "langues-vivantes-regionales": {
      "id": "langues-vivantes-regionales",
      "categoryId": 134,
      "label": "Langues vivantes régionales"
    },
    "neerlandais": {
      "id": "neerlandais",
      "categoryId": 137,
      "label": "Néerlandais"
    },
    "norvegien": {
      "id": "norvegien",
      "categoryId": 140,
      "label": "Norvégien"
    },
    "polonais": {
      "id": "polonais",
      "categoryId": 143,
      "label": "Polonais"
    },
    "portugais": {
      "id": "portugais",
      "categoryId": 146,
      "label": "Portugais"
    },
    "suedois": {
      "id": "suedois",
      "categoryId": 149,
      "label": "Suédois"
    },
    "turc": {
      "id": "turc",
      "categoryId": 152,
      "label": "Turc"
    },
    "Langue des signes française": {
      "id": "langue-des-signes-francaise",
      "categoryId": 155,
      "label": "Langue des signes française (LSF)"
    }
  }
};

const enseignementProfessionnel = {
  "id": "enseignement-professionnel",
  "label": "Enseignement professionnel",
  "children": {
    "assistance-a-la-gestion": {
      "id": "assistance-a-la-gestion",
      "categoryId": 170,
      "label": "Assistance à la gestion"
    },
    "metiers-du-commerce-et-de-la-vente": {
      "id": "metiers-du-commerce-et-de-la-vente",
      "categoryId": 173,
      "label": "Métiers du commerce et de la vente"
    },
    "filieres-securite": {
      "id": "filieres-securite",
      "categoryId": 179,
      "label": "Filières sécurité"
    },
    "metiers-de-laccueil": {
      "id": "metiers-de-laccueil",
      "categoryId": 176,
      "label": "Métiers de l'accueil"
    },
    "metiers-du-transport-et-de-la-logistique": {
      "id": "metiers-du-transport-et-de-la-logistique",
      "categoryId": 182,
      "label": "Métiers du transport et de la logistique"
    },
    "metiers-de-lhotellerie-restauration": {
      "id": "metiers-de-lhotellerie-restauration",
      "categoryId": 183,
      "label": "Métiers de l'hôtellerie-restauration"
    },
    "metiers-de-lalimentation": {
      "id": "metiers-de-lalimentation",
      "categoryId": 180,
      "label": "Métiers de l'alimentation"
    },
    "sciences-biologiques-sbssa": {
      "id": "sciences-biologiques-sbssa",
      "categoryId": 185,
      "label": "Sciences biologiques (SBSSA)"
    },
    "sciences-et-techniques-industrielles": {
      "id": "sciences-et-techniques-industrielles",
      "categoryId": 188,
      "label": "Sciences et techniques industrielles"
    },
    "design-et-arts-appliques": {
      "id": "design-et-arts-appliques",
      "categoryId": 191,
      "label": "Design et arts appliqués"
    },
    "3eme-prepa-metiers": {
      "id": "3eme-prepa-metiers",
      "categoryId": 194,
      "label": "3ème prépa métiers"
    }
  }
};

const educationPhysiqueSportive = {
  "id": "education-physique-sprotive",
  "label": "Education physique & sportive",
  "children": {
    "produire-performance-optimale": {
      "id": "produire-performance-optimale",
      "categoryId": 197,
      "label": "Produire une performance optimale"
    },
    "adapter-ses-deplacements": {
      "id": "adapter-ses-deplacements",
      "categoryId": 200,
      "label": "Adapter ses déplacements"
    },
    "exprimer-devant-les-autres": {
      "id": "exprimer-devant-les-autres",
      "categoryId": 203,
      "label": "S’exprimer devant les autres"
    },
    "conduire-et-maitriser-un-affrontement": {
      "id": "conduire-et-maitriser-un-affrontement",
      "categoryId": 206,
      "label": "Conduire et maîtriser un affrontement"
    },
    "realiser-une-activite-physique": {
      "id": "realiser-une-activite-physique",
      "categoryId": 209,
      "label": "Réaliser une activité physique"
    }
  }
};

export default {
  "id": "2d",
  "label": "Second degré",
  "hint": "Cliquez pour afficher les rubriques du second degré",
  "children": {
    "arts-lettres-sciences-humaines": artsLettresSciencesHumaines,
    "sciences-technologies": sciencesTechnologies,
    "langues-vivantes": languesVivantes,
    "enseignement-professionnel": enseignementProfessionnel,
    "education-physique-sportive": educationPhysiqueSportive
  },
};
