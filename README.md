# peertube-front

## Liens maquettes

https://xd.adobe.com/view/89247773-1db4-41c0-808d-ec0673c20f91-dc77/?fullscreen&hints=off

## Colors

- Primary blue: #02235e

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## [Structure repetoire](https://nuxtjs.org/docs/directory-structure/nuxt)

### Maintenance du projet

Afin de maintenir le projet, il est possible d'appliquer la procédure suivante:

A la racine du projet:

```sh
yarn audit
```

Vérifier le résumé de l'audit.

```sh
npx yarn-audit-fix
```

Appliquer les fix disponibles.
