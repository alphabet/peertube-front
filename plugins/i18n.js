import Vue from 'vue';
import GetTextPlugin from 'vue-gettext';
import translations from "@/translations/fr_FR";

Vue.use(GetTextPlugin, {
  availableLanguages: {
    fr_FR: 'Français',
  },
  defaultLanguage: 'fr_FR',
  translations: translations,
  silent: true,
})