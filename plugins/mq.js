import Vue from 'vue'
import VueMq from 'vue-mq'

Vue.use(VueMq, {
  breakpoints: {
    xs: 600,
    sm: 960,
    md: 1280 - 16,
    lg: 1920 - 16,
    xl: Infinity
  }
});