# Changelog

### [1.11.1](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/compare/release/1.11.0...release/1.11.1) (2025-03-18)


### Bug Fixes

* **faq:** adjust peertube help URL ([d4634db](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/d4634db5f854ff397ef385888695374976bb231c))

## [1.11.0](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/compare/release/1.10.0...release/1.11.0) (2024-10-14)


### Features

* **categories:** add new categories in artsLettresSciencesHumaines ([8311400](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/8311400b6da864471eb531215aca74e71602007b))

## [1.10.0](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/compare/release/1.9.0...release/1.10.0) (2024-06-17)


### Features

* **categories:** add new categories in cycle 3 ([6e44615](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/6e44615ec148d792d8b71a88fd72dfa89c98d443))

## [1.9.0](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/compare/release/1.8.1...release/1.9.0) (2024-01-30)


### Features

* **version:** add version number in browser tab ([77d2bf4](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/77d2bf412e7bfd504446a9bbdade1006a4b2ca39))

### [1.8.1](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/compare/release/1.8.0...release/1.8.1) (2023-11-07)


### Bug Fixes

* **menu:** fix little problems ([ceddf62](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/ceddf622af4bbc2aa6ebf09b2d137e147c0daccd))
* **mobile:** fix user menu on mobile ([46db1c7](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/46db1c7132713d15a1953196a540265990a2eb67))
* **mobile:** fix wrong method used ([8bd1b9e](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/8bd1b9e8ef7890f24430fc2a0e9d4a8d60b63f82))

## [1.8.0](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/compare/release/1.7.3...release/1.8.0) (2023-07-04)


### Features

* affichage des tutos aléatoires parmi tutorials peertube ([4fad1e5](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/4fad1e545dbb3b78b89ddf181c555259f4ed7e1a))
* **docker:** build image with multistage `Dockerfile` ([f918e01](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/f918e015ecba5e62c496c408de80c4c3f4252b29))


### Bug Fixes

* ajout des mentions legales ([8c2af6a](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/8c2af6a96e1dc4f51b68dac238eb90cb01593e17))
* **css:** import fontawesome CSS in main CSS file ([1cab0ce](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/1cab0ce33c642fdbfcb879c921aa138df7da514e))
* résolution du problème retour en haut de page ([332cdb6](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/332cdb6319321bc701d023af57bbf14cc0e32203))


### Continuous Integration

* **build:** generate docker image ([dd6c07c](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/dd6c07c6a4fd482383f8440db8e059f2bc338f72))
* **lint:** enforce commit message ([690193b](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/690193b12315206dd81b885786d359dcc5c7d75c))
* **release:** avoid regression in `dev` branch ([0bae1bf](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/0bae1bf12476fb852f40430fd9492c0f590d9bcc))
* **release:** generate stable release and testing prerelease ([053cbb6](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/053cbb6a33f5dd949b0d2665d1a16df7a84c0089))
* **release:** tag docker image ([ea7b1e3](https://gitlab.mim-libre.fr/alphabet/peertube-front/-/commit/ea7b1e3457cfe26fe914bb0aeb099c6be2a72bab))
